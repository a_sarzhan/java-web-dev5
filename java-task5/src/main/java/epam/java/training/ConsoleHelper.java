package epam.java.training;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ConsoleHelper {
    private static final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public static void writeMessage(String message){
        System.out.println(message);
    }

    public static String readUserInput() throws IOException {
        return bufferedReader.readLine().trim();
    }

    public static void printClientWelcome(){
        writeMessage("****************************************");
        writeMessage("*  Welcome to Pizzeria \"Palmetto\"!!!   *");
        writeMessage("****************************************");
        writeMessage("");
        writeMessage("Let's start your Order...");
        writeMessage("");
    }

    public static String getNameOfPizza(int clientNumber, List<Pizza> pizzaList) throws IOException{
        String pizzaName;
        writeMessage("-----------------------------------------");
        writeMessage("Enter name of your Pizza:");
        pizzaName = readUserInput();
        if(!isLatinCharacters(pizzaName) || pizzaName.length() < 4 || pizzaName.length() > 20){
            int pizzaNumberInOrder = (pizzaList.isEmpty()) ? 1 : pizzaList.size() + 1;
            pizzaName = clientNumber + "_" + pizzaNumberInOrder;
        }
        writeMessage("");
        return pizzaName;
    }

    public static boolean isLatinCharacters(String name){
        return name != null && name.matches("^[a-zA-Z\\s]+$");
    }

    public static PizzaBaseType getPizzaBaseType() throws IOException{
        PizzaBaseType chosenPizzaBaseType;
        String clientPizzaBase;
        while(true){
            writeMessage("-----------------------------------------");
            writeMessage("Enter pizza base [" + PizzaBaseType.pizzaBaseTypesToString() + "]:");
            try {
                clientPizzaBase = readUserInput();
                chosenPizzaBaseType = PizzaBaseType.valueOf(clientPizzaBase.toUpperCase());
                break;

            }catch(IllegalArgumentException e){
                writeMessage("Invalid pizza base! Try again");
            }
        }
        writeMessage("");
        return chosenPizzaBaseType;
    }

    public static int getPizzaQuantity() throws IOException{
        int pizzaQuantity;
        while(true) {
            try {
                writeMessage("-----------------------------------------");
                writeMessage("Enter pizza quantity:");
                pizzaQuantity = Integer.parseInt(readUserInput());
                if(pizzaQuantity > 10){
                    writeMessage("Invalid quantity. Maximum 10 same pizzas allowed!");
                    continue;
                }
                if(pizzaQuantity <= 0){
                    writeMessage("Negative or Zero quantity. Try again");
                    continue;
                }
                break;
            }catch(NumberFormatException e){
                writeMessage("Invalid quantity! Try again");
            }
        }
        writeMessage("");
        return pizzaQuantity;
    }

    public static void amendPizzaQuantityByClient(Order order) throws IOException{
        String pizzaName;
        boolean isNameCorrect;
        while(true) {
            isNameCorrect = false;
            writeMessage("-----------------------------------------");
            writeMessage("Your order:");
            order.printPizzaAttributes();
            writeMessage("-----------------------------------------");

            writeMessage("Enter pizza name, to change it's quantity:");
            writeMessage("Enter \"exit\" to finish");
            pizzaName = readUserInput();
            if(pizzaName.equalsIgnoreCase("exit"))
                break;
            for (Pizza pizza : order.getPizzaArray()) {
                if(pizza.getPizzaName().equalsIgnoreCase(pizzaName)){
                    order.modifyPizzaQuantity(pizza.getPizzaName(), getPizzaQuantity());
                    isNameCorrect = true;
                    break;
                }
            }
            if(!isNameCorrect)
                writeMessage("Incorrect pizza name! Try again");
        }
    }

    public static void initializeIngredients(Pizza thisPizza) throws IOException{
        String ingredient;
        Ingredients chosenIngredient;
        List<Ingredients> ingredientList = new ArrayList<>();
        writeMessage("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        writeMessage("Let's add ingredients below to your Pizza one by one");
        writeMessage("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        while(true) {
            writeMessage("[" + Ingredients.ingredientsListToString() + "]");
            writeMessage("-----------------------------------------");
            writeMessage("Enter ingredient:");
            writeMessage("Enter \"exit\" to finish");
            try {
                ingredient = readUserInput().replaceAll("\\s+", "_");
                if(ingredient.equalsIgnoreCase("exit"))
                    break;
                chosenIngredient = Ingredients.valueOf(ingredient.toUpperCase());
                if(ingredientList.size() >= 7){
                    writeMessage("Your pizza is full of ingredients!!!");
                    break;
                }
                if(ingredientList.contains(chosenIngredient)){
                    writeMessage("Ingredient recurrence! Check your order");
                }else{
                    ingredientList.add(chosenIngredient);
                    thisPizza.addIngredient(chosenIngredient);
                }
            } catch (IllegalArgumentException e) {
                writeMessage("Invalid ingredient! Please try again");
            }
        }

    }

}
