package epam.java.training;

import java.sql.SQLOutput;

public enum PizzaBaseType {
    REGULAR("Pizza Base (Regular)", 1),
    CALZONE("Pizza Base (Calzone)", 1.5);

    String displayName;
    double cost;
    PizzaBaseType(String displayName, double cost){
        this.displayName = displayName;
        this.cost = cost;
    }

    public static String pizzaBaseTypesToString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(REGULAR.toString().toLowerCase() + ", ");
        stringBuilder.append(CALZONE.toString().toLowerCase());
        return stringBuilder.toString();
    }
}
