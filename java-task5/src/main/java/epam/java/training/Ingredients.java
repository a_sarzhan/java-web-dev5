package epam.java.training;

public enum Ingredients {
    TOMATO_PASTE("Tomato Paste", 1),
    CHEESE("Cheese", 1),
    SALAMI("Salami", 1.5),
    BACON("Bacon", 1.2),
    GARLIC("Garlic", 0.3),
    CORN("Corn", 0.7),
    PEPPERONI("Pepperoni", 0.6),
    OLIVES("Olives", 0.5);

    String name;
    double cost;

    Ingredients(String name, double cost) {
        this.name = name;
        this.cost = cost;
    }

    public static String ingredientsListToString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(TOMATO_PASTE.name + ", ");
        stringBuilder.append(CHEESE.name + ", ");
        stringBuilder.append(SALAMI.name + ", ");
        stringBuilder.append(BACON.name + ", ");
        stringBuilder.append(GARLIC.name + ", ");
        stringBuilder.append(CORN.name + ", ");
        stringBuilder.append(PEPPERONI.name + ", ");
        stringBuilder.append(OLIVES.name);
        return stringBuilder.toString();
    }
}
