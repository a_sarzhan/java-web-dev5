package epam.java.training;

import java.io.IOException;
import java.time.LocalTime;
import java.util.*;

/**
 * Epam - Java Web Development Training
 * Assel Sarzhanova
 * Task 5
 */
public class App {
    public static void main( String[] args ) throws IOException {
        List<Pizza> clientPizzaList = new ArrayList<>();
        List<Integer> clientList = new ArrayList<>();
        clientList.add(7717);
        clientList.add(4372);

        for (int clientId : clientList) {

            ConsoleHelper.printClientWelcome();
            while (true) {
                String pizzaName = ConsoleHelper.getNameOfPizza(clientId, clientPizzaList);
                PizzaBaseType pizzaBaseType = ConsoleHelper.getPizzaBaseType();
                int currentPizzaQuantity = ConsoleHelper.getPizzaQuantity();
                Pizza currentPizza = new Pizza(pizzaName, pizzaBaseType, currentPizzaQuantity);
                ConsoleHelper.initializeIngredients(currentPizza);

                clientPizzaList.add(currentPizza);

                ConsoleHelper.writeMessage("Continue with order? Y/N:");
                if (ConsoleHelper.readUserInput().equalsIgnoreCase("n"))
                    break;
            }
            Pizza[] pizzaListToArray = new Pizza[clientPizzaList.size()];
            Order clientOrder = new Order(clientId, clientPizzaList.toArray(pizzaListToArray), LocalTime.now());
            ConsoleHelper.amendPizzaQuantityByClient(clientOrder);
            System.out.println(clientOrder.toString());

            clientPizzaList.clear();
        }
    }

}
