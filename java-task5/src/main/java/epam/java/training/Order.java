package epam.java.training;


import java.time.LocalTime;

public class Order {
    private static int counter_start = 10000;
    private final int orderId;
    private int clientId;
    private Pizza[] pizzaArray;
    private LocalTime currentTime;

    public Order(int clientId, Pizza[] pizzaArray, LocalTime time){
        this.orderId = ++counter_start;
        this.clientId = clientId;
        this.pizzaArray = pizzaArray;
        this.currentTime = time;
    }


    public void modifyPizzaQuantity(String pizzaName, int quantity){
        for (int i = 0; i < pizzaArray.length; i++) {
            if(pizzaArray[i].getPizzaName().equalsIgnoreCase(pizzaName)){
                pizzaArray[i].setPizzaQuantity(quantity);
                break;
            }
        }
    }

    public void printPizzaAttributes(){
        for (int i = 0; i < pizzaArray.length; i++) {
            String pizzaAttributes = String.format("%d : %d : %s : %d", orderId, clientId, pizzaArray[i].getPizzaName(), pizzaArray[i].getPizzaQuantity());
            ConsoleHelper.writeMessage(pizzaAttributes);
        }
    }

    public Pizza[] getPizzaArray() {
        return pizzaArray;
    }

    @Override
    public String toString() {
        double subTotal = 0;
        double totalSum = 0;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("********************************************\n");
        stringBuilder.append("Order: " + orderId + "\n");
        stringBuilder.append("Client ID: " + clientId + "\n");
        for (int i = 0; i < pizzaArray.length; i++) {
            stringBuilder.append("Name: " + pizzaArray[i].getPizzaName() + "\n");
            stringBuilder.append("--------------------------------------------\n");
            stringBuilder.append(String.format("%-20s %20.2f \u20ac\n", pizzaArray[i].getPizzaType().displayName, pizzaArray[i].getPizzaType().cost));

            subTotal += pizzaArray[i].getPizzaType().cost;
            for (Ingredients ingredient: pizzaArray[i].getIngredients()) {
                if(ingredient != null) {
                    stringBuilder.append(String.format("%-20s %20.2f \u20ac\n", ingredient.name, ingredient.cost));

                    subTotal += ingredient.cost;
                }
            }
            stringBuilder.append("--------------------------------------------\n");
            stringBuilder.append(String.format("%-20s %20.2f \u20ac\n", "Sub Total:", subTotal));
            stringBuilder.append(String.format("%-20s %20d \n", "Quantity:", pizzaArray[i].getPizzaQuantity()));
            stringBuilder.append("--------------------------------------------\n");
            totalSum += subTotal * pizzaArray[i].getPizzaQuantity();
            subTotal = 0;
        }
        stringBuilder.append(String.format("%-20s %20.2f \u20ac\n", "TOTAL DUE:", totalSum));
        stringBuilder.append("********************************************\n");

        return stringBuilder.toString();
    }
}
