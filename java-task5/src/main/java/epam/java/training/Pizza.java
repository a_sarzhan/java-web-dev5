package epam.java.training;

public class Pizza {
    private String pizzaName;
    private PizzaBaseType pizzaType;
    private int pizzaQuantity;
    private Ingredients[] ingredients;

    public Pizza(String pizzaName, PizzaBaseType pizzaType, int pizzaQuantity){
        this.pizzaName = pizzaName;
        this.pizzaType = pizzaType;
        this.pizzaQuantity = pizzaQuantity;
        this.ingredients = new Ingredients[7];
    }

    public void addIngredient(Ingredients clientIngredient){
        for (int i = 0; i < ingredients.length; i++) {
            if(ingredients[i] == null) {
                ingredients[i] = clientIngredient;
                break;
            }
        }
    }

    public int getPizzaQuantity() {
        return pizzaQuantity;
    }

    public String getPizzaName() {
        return pizzaName;
    }

    public PizzaBaseType getPizzaType() {
        return pizzaType;
    }

    public Ingredients[] getIngredients() {
        return ingredients;
    }

    public void setPizzaQuantity(int pizzaQuantity) {
        this.pizzaQuantity = pizzaQuantity;
    }
}
